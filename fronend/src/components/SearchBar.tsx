// components/SearchBar.tsx
import React, { useState } from "react";
interface SearchBarProps {
  onSearch: (query: string, dataset: string) => void;
  loading: boolean;
}

const SearchBar: React.FC<SearchBarProps> = ({ onSearch, loading }) => {
  const [query, setQuery] = useState("");
  const [dataset, setDataset] = useState("dataset1");

  const handleSearch = () => {
    onSearch(query, dataset);
  };

  return (
    <div className="flex items-center justify-center mt-10">
      <select
        className="px-4 py-2 text-white bg-blue-500 rounded-l-md hover:bg-blue-600 focus:outline-none  "
        value={dataset}
        onChange={(e) => setDataset(e.target.value)}
      >
        <option className="bg-blue-200 hover:bg-blue-200" value="dataset1">
          Antique
        </option>
        <option className="bg-blue-200 hover:bg-blue-200" value="dataset2">
          Life Style
        </option>
      </select>{" "}
      <input
        type="text"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        placeholder="Search..."
        className="w-80 px-4 mx-0 py-2 text-gray-800 border border-gray-300 rounded-l-md focus:outline-none focus:border-blue-500"
      />
      <button
        onClick={handleSearch}
        disabled={loading || query === ""}
        className="px-4 py-2 text-white bg-blue-500 rounded-r-md hover:bg-blue-600 disabled:bg-gray-500 focus:outline-none focus:bg-blue-600"
      >
        {!loading ? "Search" : "Loading..."}
      </button>
    </div>
  );
};

export default SearchBar;
