import React from "react";

interface SearchResult {
  document: string;
  index: string;
  score: string;
}

interface SearchResultsProps {
  results: SearchResult[];
}

const SearchResults: React.FC<SearchResultsProps> = ({ results }) => {
  return (
    <div className="mt-8">
      {results.length > 0 ? (
        results.map((result, index) => (
          <div
            key={index + 1}
            className="bg-white shadow-lg border border-gray-200 rounded-lg p-6 mb-8"
          >
            <h3 className="text-sm font-semibold text-blue-500 mb-2">
              {result.index}
            </h3>
            <p className="text-gray-600 ">{result.document}</p>
          </div>
        ))
      ) : (
        <p className="text-gray-600">No results found</p>
      )}
    </div>
  );
};

export default SearchResults;
