// pages/index.tsx
import React, { useState } from "react";
import SearchBar from "../components/SearchBar";
import SearchResults from "../components/SearchResults";

interface SearchResult {
  document: string;
  index: string;
  score: string;
}

const Home: React.FC = () => {
  const [results, setResults] = useState<SearchResult[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const handleSearch = async (query: string, dataset: string) => {
    const formData = new FormData();

    formData.append("search", query);
    formData.append("dataset", dataset);
    setLoading(true); // Start loading

    try {
      const response = await fetch("http://127.0.0.1:5000/search", {
        method: "POST",
        body: formData,
      });
      console.log(formData);

      if (response.ok) {
        const data = await response.json();
        setResults(data);
        setLoading(false); // Start loading

      } else {
        console.error("Failed to fetch search results");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };
  return (
    <div className="container mx-auto px-4">
      <SearchBar loading={loading} onSearch={handleSearch} />
      {/* todo: add loading component unteell response come  */}
      <SearchResults results={results} />
    </div>
  );
};

export default Home;
