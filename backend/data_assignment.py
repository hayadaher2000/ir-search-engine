import csv
########## SERVICE FOR STORING THE CLEANED CORPUS ###########
def store_docs_array_in_file(docs_array, file_name):
    with open(file_name, 'w', encoding='utf-8', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(docs_array)
######### READ IT FROM THE FILE
def read_docs_array_from_file(file_name):
    with open(file_name, 'r') as file:
        reader = csv.reader(file)
        docs_array = []
        for row in reader:
            docs_array.append(row)
    return docs_array