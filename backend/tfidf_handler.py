import nltk

import numpy as np
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle
from sklearn.metrics.pairwise import cosine_similarity
from scipy.sparse import csr_matrix, load_npz

from nltk.corpus import wordnet

stemmer = PorterStemmer()


def check_word_exist_in_doc(word, docs_array):
    for doc in docs_array:
        for term in doc:
            if word == term:
                return True
    return False


def expand_query(query_array, docs_array):
    synonyms = []
    for word in query_array:
        word_synonyms = set()
        for syn in wordnet.synsets(word):
            for lemma in syn.lemmas():
                if check_word_exist_in_doc(lemma.name(), docs_array):
                    word_synonyms.add(lemma.name())
        word_synonyms = sorted(word_synonyms, key=lambda x: nltk.edit_distance(word, x))[:3]
        synonyms.append(word_synonyms)

    new_synonyms = []
    for syn in synonyms:
        for term in syn:
            if syn not in query_array:
                new_synonyms.append(term)
    expand_query = " ".join(query_array) + " " + " ".join(new_synonyms)
    return expand_query


def make_tf_idf_values(docs_array):
    vectorizer = TfidfVectorizer()
    tfidf_matrix = vectorizer.fit_transform([' '.join(doc) for doc in docs_array])
    return vectorizer.get_feature_names_out(), tfidf_matrix, vectorizer


######## LOAD THE MATRIX (USED IN INLINE MODE) ##############
def load_vect_and_tfidf(tfidf_file, vectorizer_file):
    tfidf_matrix = load_npz(tfidf_file)
    with open(vectorizer_file, 'rb') as f:
        vectorizer = pickle.load(f)
    return tfidf_matrix, vectorizer


def get_query_result(query_array, vectorizer, tfidf, expand_query):
    q = " ".join(query_array)
    ex_q = " ".join(expand_query)

    query_vec = vectorizer.transform([q])
    expanded_query_vector = vectorizer.transform([ex_q])

    alpha = 0.7
    beta = 0.3

    weighted_query_vector = alpha * query_vec + beta * expanded_query_vector
    cosine_similarities = cosine_similarity(weighted_query_vector, tfidf)
    sorted_doc_indices = np.argsort(cosine_similarities[0])[-10:]
    sorted_scores = cosine_similarities[0][sorted_doc_indices]
    return sorted_doc_indices, sorted_scores


