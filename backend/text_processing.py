import gzip
import json
from datetime import datetime
from collections import defaultdict
import nltk
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, WordNetLemmatizer
import re
from dateutil import parser
import ir_datasets

stemmer = PorterStemmer()

dataset = ir_datasets.load("antique/test")
dataset2 = ir_datasets.load("lotte/lifestyle/dev/forum")

documents = []
documents_keys = []

documents2 = []
documents_keys2 = []


def assign_data_set_to_documents():
    for i, doc in enumerate(dataset.docs_iter()):
        documents.append(doc[1])
        documents_keys.append(doc[0])


def assign_data_set_to_documents2():
    for i, doc in enumerate(dataset2.docs_iter()):
        documents2.append(doc[1])
        documents_keys2.append(doc[0])


def remove_whitespace(text):
    return " ".join(text.split())


def remove_stopwords(words):
    stop_words = set(stopwords.words("english"))
    filtered_text = [word for word in words if word not in stop_words]
    return filtered_text


def stem_words(words):
    stems = [stemmer.stem(word) for word in words]
    return stems


def remove_lemma(words):
    tagged_words = nltk.pos_tag(words)
    lemmatizer = WordNetLemmatizer()
    lemmatized_words = []
    for word, tag in tagged_words:
        lemma = lemmatizer.lemmatize(word)
        lemmatized_words.append(lemma)
    return lemmatized_words


def is_date_or_time(word):
    try:
        parser.parse(word)
        return True
    except:
        return False


def remove_punctuation(words):
    new_words = []
    # Remove punctuation
    for word in words:
        if word.isalpha() or is_date_or_time(word):
            new_words.append(word)
    return new_words


def text_lowercase(text):
    return text.lower()


def format_dates(document):
    # Define a regular expression pattern to match different date formats
    date_pattern = re.compile(r'\d{1,2}[/-]\d{1,2}[/-]\d{4}|\d{4}[/-]\d{1,2}[/-]\d{1,2}')
    time_pattern = re.compile(r'\d{1,2}:\d{2}(:\d{2})?\s*(AM|PM|am|pm)?')
    # Define the desired output date format
    output_format = "%Y-%m-%d"
    output_time_format = "%I:%M %p"
    # Replace the dates in the document with the desired output format
    for match in date_pattern.findall(document):
        try:
            date_obj = parser.parse(match)
            standard_date_format = date_obj.strftime(output_format)
            document = document.replace(match, standard_date_format)
        except ValueError:
            continue
    try:
        document = time_pattern.sub(
            lambda match: datetime.strptime(match.group(), '%I:%M %p' if match.group(2) else '%H:%M').strftime(
                output_time_format), document)
    except ValueError:
        print("")
    return document


def process_shortcuts(document):
    matches = []
    shortcut_dict = {
        'p.p.s': 'post postscript',
        'u.s.a': 'united states of america',
        'a.k.a': 'also known as',
        'm.a.d': 'Mutually Assured Destruction',
        'a.b.b': 'Asea Brown Boveri',
        's.c.o': 'Santa Cruz Operation',
        'e.t.c': 'etcetera',
        'm.i.t': 'Massachusetts Institute of Technology',
        'v.i.p': 'very important person',
        'us': 'united states of america',
        'u.s.': 'united states of america',
        'usa': 'united states of america',
        'cobol': 'common business oriented language',
        'rpm': 'red hat package manager',
        'ap': 'associated press',
        'gpa': 'grade point average',
        'npr': 'national public radio',
        'fema': 'federal emergency',
        'crt': 'cathode ray tube',
        'gm': 'grandmaster',
        'fps': 'frames per second',
        'pc': 'personal computer',
        'pms': 'premenstrual syndrome',
        'cia': 'central intelligence agency',
        'aids': 'acquired immune deficiency syndrome',
        'it\'s': 'it is',
        'you\'ve': 'you have',
        'what\'s': 'what is',
        'that\'s': 'that is',
        'who\'s': 'who is',
        'don\'t': 'do not',
        'haven\'t': 'have not',
        'there\'s': 'there is',
        'i\'d': 'i would',
        'it\'ll': 'it will',
        'i\'m': 'i am',
        'here\'s': 'here is',
        'you\'ll': 'you will',
        'cant\'t': 'can not',
        'didn\'t': 'did not',
        'hadn\'t': 'had not',
        'kv': 'kilovolt',
        'cc': 'cubic centimeter',
        'aoa': 'american osteopathic association',
        'rbi': 'reserve bank',
        'pls': 'please',
        'dvd': 'digital versatile disc',
        'bdu': 'boise state university',
        'dvd': 'digital versatile disc',
        'mac': 'macintosh',
        'tv': 'television',
        'cs': 'computer science',
        'cse': 'computer science engineering',
        'iit': 'indian institutes of technology',
        'uk': 'united kingdom',
        'eee': 'electrical and electronics engineering',
        'ca': 'california',
        'etc': 'etcetera',
        'ip': 'internet protocol',
        'bjp': 'bharatiya janata party',
        'gdp': ' gross domestic product',
        'un': 'unitednations',
        'ctc': 'cost to company',
        'atm': 'automated teller machine',
        'pvt': 'private',
        'iim': 'indian institutes of management'

    }
    shortcut_pattern1 = re.compile(r'[A-Za-z]\.[A-Za-z]\.[A-Za-z]*')
    shortcut_pattern2 = re.compile(r'\b[A-Za-z]{2,3}\b')
    shortcut_pattern3 = re.compile(r'\w+\'\w+')

    matches.append(shortcut_pattern1.findall(document))
    matches.append(shortcut_pattern2.findall(document))
    matches.append(shortcut_pattern3.findall(document))

    for arr in matches:
        for match in arr:
            if match in shortcut_dict:
                document = document.replace(match, shortcut_dict[match])

    return document


def text_processing(choosed_document):
    docs_array = []
    for i, document in enumerate(choosed_document):
        document = remove_whitespace(document)
        document = text_lowercase(document)
        document = process_shortcuts(document)
        document = format_dates(document)
        words = word_tokenize(document)
        words = remove_punctuation(words)
        words = remove_stopwords(words)
        words = remove_lemma(words)
        words = stem_words(words)
        docs_array.append(words)
    return docs_array


def make_inverted_index(docs_array):
    inverted_index = defaultdict(list)
    for i, doc in enumerate(docs_array):
        for token in doc:
            if i not in inverted_index[token]:
                inverted_index[token].append(i)
    return dict(inverted_index)


def store_inverted_index(inverted_index, file_name):
    json_data = json.dumps(inverted_index)
    with gzip.open(file_name, 'wt') as f:
        f.write(json_data)


def process_query(query):
    query_array = []
    query = remove_whitespace(query)
    query = text_lowercase(query)
    query = process_shortcuts(query)
    query = format_dates(query)
    # query = error_detection(query)
    words = word_tokenize(query)
    words = remove_punctuation(words)
    words = remove_stopwords(words)
    words = remove_lemma(words)
    words = stem_words(words)
    query_array = words
    return query_array


def check_word_exist_in_doc(word, docs_array):
    for doc in docs_array:
        for term in doc:
            if word == term:
                return True
    return False


def expand_query(query_array, docs_array):
    synonyms = []
    for word in query_array:
        word_synonyms = set()
        for syn in wordnet.synsets(word):
            for lemma in syn.lemmas():
                if check_word_exist_in_doc(lemma.name(), docs_array):
                    word_synonyms.add(lemma.name())
        word_synonyms = sorted(word_synonyms, key=lambda x: nltk.edit_distance(word, x))[:3]
        synonyms.append(word_synonyms)

    new_synonyms = []
    for syn in synonyms:
        for term in syn:
            if syn not in query_array:
                new_synonyms.append(term)
    expand_query = " ".join(query_array) + " " + " ".join(new_synonyms)
    return expand_query


def get_documents(sorted_doc_indices, sorted_scores, dataset_type):
    ranked_doc = []
    cnt = len(sorted_scores) - 1
    for idx in sorted_doc_indices:
        if dataset_type == 1:
            element = {
                "document": documents[idx],
                "socre": '{:.3f}'.format(sorted_scores[cnt]),
                "index": documents_keys[idx]
            }
        else:
            element = {
                "document": documents2[idx],
                "socre": '{:.3f}'.format(sorted_scores[cnt]),
                "index": documents_keys2[idx]
            }
        cnt -= 1
        ranked_doc.append(element)
    return ranked_doc
