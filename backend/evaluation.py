import ir_datasets
from data_assignment import read_docs_array_from_file
from text_processing import process_query, expand_query, get_documents, documents_keys2, documents_keys
from tfidf_handler import get_query_result, load_vect_and_tfidf

dataset = ir_datasets.load("antique/test")
dataset2 = ir_datasets.load("lotte/lifestyle/dev/forum")

queries = []
queries2 = []

docs_array2 = read_docs_array_from_file('files/docs_array2.csv')
docs_array = read_docs_array_from_file('files/docs_array.csv')


tfidf_matrix2, vectorizer2 = load_vect_and_tfidf("files/tfidf_matrix2.npz", "files/vectorizer2.pkl")
tfidf_matrix, vectorizer = load_vect_and_tfidf("files/tfidf_matrix.npz", "files/vectorizer.pkl")


def get_queries():
    global queries
    counter = 0
    for query in dataset.queries_iter():
        if counter >= 50:
            break
        counter += 1
        queries.append(query)


def get_queries2():
    global queries2
    counter = 0
    for query in dataset2.queries_iter():
        if counter >= 50:
            break
        counter += 1
        queries2.append(query)


qrels = []
qrels2 = []


def get_qrels():
    for qrel in dataset.qrels_iter():
        query_id = qrel[0]
        if query_id in [query[0] for query in queries]:
            qrels.append(qrel)


def get_qrels2():
    for qrel in dataset2.qrels_iter():
        query_id = qrel[0]
        if query_id in [query[0] for query in queries2]:
            qrels2.append(qrel)


retrieved_docs = {}
retrieved_docs2 = {}


def get_retrieved_documents(datasets_type):
    if datasets_type == 1:
        for query in queries:
            q = query.text
            retrieved_docs[query.query_id] = {}
            query_array = process_query(q)
            expanded_query = expand_query(query_array, docs_array)
            sorted_doc_indices, sorted_scores = get_query_result(query_array, vectorizer, tfidf_matrix, expanded_query)
            sorted_docs_with_scores = get_documents(sorted_doc_indices, sorted_scores, 1)
            retrieved_docs[query.query_id] = sorted_docs_with_scores
    else:
        for query in queries2:
            q = query.text
            retrieved_docs2[query.query_id] = {}
            query_array = process_query(q)
            expanded_query = expand_query(query_array, docs_array2)
            sorted_doc_indices, sorted_scores = get_query_result(query_array, vectorizer2, tfidf_matrix2,
                                                                 expanded_query)
            sorted_docs_with_scores = get_documents(sorted_doc_indices, sorted_scores, 2)
            retrieved_docs2[query.query_id] = sorted_docs_with_scores


def get_QRELS(datasets_type):
    retrievedRelevantDocs = {}
    if datasets_type == 1:
        for qrel in qrels:
            if qrel.query_id not in retrievedRelevantDocs:
                retrievedRelevantDocs[qrel.query_id] = []
            retrievedRelevantDocs[qrel.query_id].append({'relevance': qrel.relevance, 'doc_id': qrel.doc_id})
    else:
        for qrel in qrels2:
            if qrel.query_id not in retrievedRelevantDocs:
                retrievedRelevantDocs[qrel.query_id] = []
            retrievedRelevantDocs[qrel.query_id].append({'relevance': qrel.relevance, 'doc_id': qrel.doc_id})
    return retrievedRelevantDocs


def get_relevance_non_relevance_docs(relevantDocs, datasets_type):
    qrels = {}
    threshold = 1
    for qrel in relevantDocs:
        # Retrieve the query ID and relevant documents
        query_id = qrel
        relevant_docs = relevantDocs[query_id]
        # Add the query ID to the qrels dictionary
        qrels[query_id] = []
        # Iterate over all the documents in the dataset and add them to the qrels dictionary
        if datasets_type == 1:
            for doc_id in documents_keys:
                relevance = 2
                if doc_id in [d['doc_id'] for d in relevant_docs]:
                    relevance = 1
                else:
                    relevance = 0

                # Add the document and relevance score to the qrels dictionary
                qrels[query_id].append({'doc_id': doc_id, 'relevance': relevance})
        else:
            for doc_id in documents_keys2:
                relevance = 2
                if doc_id in [d['doc_id'] for d in relevant_docs]:
                    relevance = 1
                else:
                    relevance = 0

                # Add the document and relevance score to the qrels dictionary
                qrels[query_id].append({'doc_id': doc_id, 'relevance': relevance})
    return qrels


def getRelevance1(query_id, qrels_new):
    relevance1 = set()
    for doc in qrels_new.get(query_id):
        if (doc['relevance'] == 1):
            relevance1.add(doc['doc_id'])
    return relevance1


def getRetrievedDocs(retrieved):
    retrievedDocs = set()
    for doc in retrieved:
        retrievedDocs.add(doc['index'])
    return retrievedDocs


# measures

def precission_at_10(relevance, retrieved):
    num_relevant_retrieved = len(set(relevance).intersection(retrieved))
    precision_at_10 = num_relevant_retrieved / 10
    return precision_at_10


def recall_values(relevance, retrieved):
    num_relevant_retrieved = len(relevance.intersection(retrieved))
    num_relevant_total = len(relevance)
    recall = num_relevant_retrieved / num_relevant_total
    return recall


def mean_avg_precision(relevance1, retrieved):
    precision_sum = 0.0
    num_relevant = len(relevance1)
    num_correct = 0
    for i, doc in enumerate(retrieved):
        if doc in relevance1:
            num_correct += 1
            precision = num_correct / (i + 1)
            precision_sum += precision

    ap = precision_sum / num_relevant
    return ap


def mean_reciprocal_rank(relevance1, retrieved):
    rr = 0
    for i, doc in enumerate(retrieved):
        if doc in relevance1:
            rr = 1 / (i + 1)
            break
    return rr


def calc_evaluation(qrels_new):
    AP = []
    MRR = []

    for query in queries:
        relevance1 = getRelevance1(query.query_id, qrels_new)
        retrieved = getRetrievedDocs(retrieved_docs[query.query_id])
        # recall
        r = recall_values(relevance1, retrieved)
        #     #precission @ 10
        p = precission_at_10(relevance1, retrieved)
        with open('files/evaluation.txt', 'a') as f:
            f.write(f"{query.query_id}: precision@k:{p:.3f} recall:{r:.3f}\n")

        ap = mean_avg_precision(relevance1, retrieved)
        AP.append(ap)

        rr = mean_reciprocal_rank(relevance1, retrieved)
        MRR.append(rr)
    # MRR
    mean_MRR = sum(MRR) / len(MRR)
    # MAP
    MAP = sum(AP) / len(AP)
    with open('files/evaluation.txt', 'a') as f:
        f.write(f"{query.query_id}: MRR:{mean_MRR:.3f} MAP:{MAP:.3f}\n")


def calc_evaluation2(qrels_new):
    AP = []
    MRR = []

    for query in queries2:
        relevance1 = getRelevance1(query.query_id, qrels_new)
        retrieved = getRetrievedDocs(retrieved_docs2[query.query_id])
        # recall
        r = recall_values(relevance1, retrieved)
        #     #precission @ 10
        p = precission_at_10(relevance1, retrieved)
        with open('files/evaluation2.txt', 'a') as f:
            f.write(f"{query.query_id}: precision@k:{p:.3f} recall:{r:.3f}\n")

        ap = mean_avg_precision(relevance1, retrieved)
        AP.append(ap)

        rr = mean_reciprocal_rank(relevance1, retrieved)
        MRR.append(rr)
    # MRR
    mean_MRR = sum(MRR) / len(MRR)
    # MAP
    MAP = sum(AP) / len(AP)
    with open('files/evaluation2.txt', 'a') as f:
        f.write(f"{query.query_id}: MRR:{mean_MRR:.3f} MAP:{MAP:.3f}\n")

