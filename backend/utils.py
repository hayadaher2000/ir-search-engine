from text_processing import process_query

def read_queries_from_file(file_name):
    queries = []
    with open(file_name, 'r') as file:
        for line in file:
            query_id, query_text = line.strip().split('\t')
            queries.append({"query_id": query_id, "text": query_text})
    return queries

def read_relevant_docs_from_file(file_name):
    relevant_docs = {}
    with open(file_name, 'r') as file:
        for line in file:
            query_id, doc_id = line.strip().split('\t')
            if query_id not in relevant_docs:
                relevant_docs[query_id] = []
            relevant_docs[query_id].append(doc_id)
    return relevant_docs

