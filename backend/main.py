from flask import Flask, request, render_template, jsonify
from flask_cors import CORS
from data_assignment import read_docs_array_from_file
from evaluation import get_retrieved_documents
from text_processing import process_query, expand_query,assign_data_set_to_documents,assign_data_set_to_documents2,get_documents
from tfidf_handler import get_query_result,  load_vect_and_tfidf

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

# Load Datasets

assign_data_set_to_documents()
assign_data_set_to_documents2()




# Load documents
docs_array = read_docs_array_from_file('files/docs_array.csv')
docs_array2 = read_docs_array_from_file('files/docs_array2.csv')


# Load TFIDF matrices and vectorizers
tfidf_matrix2, vectorizer2 = load_vect_and_tfidf("files/tfidf_matrix2.npz", "files/vectorizer2.pkl")
tfidf_matrix, vectorizer = load_vect_and_tfidf("files/tfidf_matrix.npz", "files/vectorizer.pkl")


@app.route('/search', methods=['POST'])
def searchText():
    query = request.form['search']
    dataset = request.form['dataset']
    query_array = process_query(query)
    # expand the query

    sorted_docs_with_scores = []

    if dataset == 'dataset1':
        # antique dataset
        expanded_query = expand_query(query_array, docs_array)
        sorted_doc_indices, sorted_scores = get_query_result(query_array, vectorizer, tfidf_matrix, expanded_query)
        sorted_docs_with_scores = get_documents(sorted_doc_indices, sorted_scores, 1)

    else:  # dataset2
        expanded_query = expand_query(query_array, docs_array2)
        sorted_doc_indices, sorted_scores = get_query_result(query_array, vectorizer2, tfidf_matrix2, expanded_query)
        sorted_docs_with_scores = get_documents(sorted_doc_indices, sorted_scores, 2)
    print(sorted_docs_with_scores)
    return jsonify(sorted_docs_with_scores)


if __name__ == '__main__':
    app.run()
