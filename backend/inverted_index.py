import nltk

class InvertedIndex:
    def __init__(self):
        self.index = {}
    
    def build_inverted_index(self, docs_array):
        for doc_id, doc in enumerate(docs_array):
            for term in doc:
                if term not in self.index:
                    self.index[term] = []
                self.index[term].append(doc_id)

    def get_postings_list(self, term):
        return self.index.get(term, [])

    def add_document(self, document):
        doc_id = len(self.index)
        for term in document:
            if term not in self.index:
                self.index[term] = []
            self.index[term].append(doc_id)
